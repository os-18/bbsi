# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright notice
# and this notice are preserved.
# This file is offered as-is, without any warranty.

SCRIPTS=$(shell find source/*)

DESTDIR=
PREFIX=usr/local
INST_DIR=$(DESTDIR)/$(PREFIX)
INST_BIN_DIR=$(INST_DIR)/bin

.PHONY: all install uninstall

all:
	@echo "All prepared."

install:
	mkdir -p "$(INST_BIN_DIR)/"
	cp source/bbsi_version    "$(INST_BIN_DIR)/"
	cp source/ff-add-subs     "$(INST_BIN_DIR)/"
	cp source/ff-concat       "$(INST_BIN_DIR)/"
	cp source/ff-cut-and-save "$(INST_BIN_DIR)/"
	cp source/ff-cut-off      "$(INST_BIN_DIR)/"
	cp source/ff-h265         "$(INST_BIN_DIR)/"
	cp source/ff-mp4          "$(INST_BIN_DIR)/"
	cp source/ff-save-frame   "$(INST_BIN_DIR)/"
	cp source/yt              "$(INST_BIN_DIR)/"
	ln -s -f yt "$(INST_BIN_DIR)/yt-a"
	ln -s -f yt "$(INST_BIN_DIR)/yt-360"
	ln -s -f yt "$(INST_BIN_DIR)/yt-480"
	ln -s -f yt "$(INST_BIN_DIR)/yt-720"
	ln -s -f yt "$(INST_BIN_DIR)/yt-1080"
	ln -s -f yt "$(INST_BIN_DIR)/yt-1440"
	ln -s -f yt "$(INST_BIN_DIR)/yt-2160"

uninstall:
	rm -f "$(INST_BIN_DIR)/bbsi_version"
	rm -f "$(INST_BIN_DIR)/ff-add-subs"
	rm -f "$(INST_BIN_DIR)/ff-concat"
	rm -f "$(INST_BIN_DIR)/ff-cut-and-save"
	rm -f "$(INST_BIN_DIR)/ff-cut-off"
	rm -f "$(INST_BIN_DIR)/ff-h265"
	rm -f "$(INST_BIN_DIR)/ff-mp4"
	rm -f "$(INST_BIN_DIR)/ff-save-frame"
	rm -f "$(INST_BIN_DIR)/yt"
	rm -f "$(INST_BIN_DIR)/yt-a"
	rm -f "$(INST_BIN_DIR)/yt-360"
	rm -f "$(INST_BIN_DIR)/yt-480"
	rm -f "$(INST_BIN_DIR)/yt-720"
	rm -f "$(INST_BIN_DIR)/yt-1080"
	rm -f "$(INST_BIN_DIR)/yt-1440"
	rm -f "$(INST_BIN_DIR)/yt-2160"

clean:
	@echo "Nothing to do."
