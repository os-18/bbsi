## BBSI

The BBSI project provides several scripts to facilitate some tasks.

This project is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
merchantability or fitness for a particular purpose.

---

## Content

| Script          | Purpose                                 |
|-----------------|-----------------------------------------|
| ff-add-subs     | Adds subtitles to videofile.            |
| ff-concat       | Merges mediafiles to a new one.         |
| ff-cut-and-save | Saves some fragment from mediafile.     |
| ff-cut-off      | Deletes some fragment from mediafile.   |
| ff-h265         | Re-encodes video using the H.265 codec. |
| ff-mp4          | Converts to MP4.                        |
| ff-save-frame   | Saves some frame from video.            |
| yt              | Small wrapper for yt-dlp.               |

Installed project provides symlinks `yt-360`, `yt-480`, `yt-720`, `yt-1080`,
`yt-1440`, `yt-2160` for downloading (using `yt`) with needed quality.  
`yt-a` downloads audio track only.


See the help for each script for details.

---

## Dependencies

This project uses
[bash](https://www.gnu.org/software/bash/),
[ffmpeg](https://ffmpeg.org/),
[yt-dlp](https://github.com/yt-dlp/yt-dlp).

---

## Ready-made packages

See:  
[Download Page of OS-18](https://codeberg.org/os-18/os-18-docs/src/branch/master/OS-18_Packages.md)  

The page contains information about installing several packages, including BBSI.


---

## Installation from sources

```bash
make install
```

Scripts will be installed to `/usr/local/bin` (`bin` is a directory for
programs, `PREFIX` is `usr/local`).

The Makefile contains variables:

* `DESTDIR` specifies the root directory for installing (empty, by default);
* `PREFIX` points to the base directory like `usr/local` or `usr`.

Installation directory is defined as `$(DESTDIR)/$(PREFIX)` in the Makefile.

You can install the scripts of this project to any other directory,
like `$HOME/.local` (script files will be in `$HOME/.local/bin/`):

```bash
make install DESTDIR=$HOME PREFIX=.local
```

Uninstall:

```bash
make uninstall
```

If you installed the project in an alternative directory:

```bash
make uninstall DESTDIR=$HOME PREFIX=.local
```

---

## Feedback

Questions, suggestions, comments, bugs:

**tech.vindex@gmail.com**

Also use the repository service tools.

