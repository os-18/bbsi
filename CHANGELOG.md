
# Changelog

---

## [0.3.1] - 2024-01-12

### Fixed

- The version output works correctly.

---

## [0.3.0] - 2024-01-08

### Added

- A new script for joining mediafiles: `ff-concat`.

### Changed

- MOV-files retain their extension (.mov) when re-encoded with `ff-h265`.

---

## [0.2.0] - 2023-06-15

### Added

- `yt` can download a video at 1440 and 2160 pixels high (new symlinks: `yt-1440`, `yt-2160`).

### Changed

- now `yt` always downloads the best possible audio track.

### Fixed

- Paths to install.
- Downloading a video at 1080 pixels high with `yt-1080`.

---

## [0.1.0] - 2023-06-12

### Added

- Initial version: first wrappers for `ffmpeg` and `yt-dlp`.
