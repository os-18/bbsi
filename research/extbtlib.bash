#!/bin/bash

# Parses CLI arguments, searches values after required option.
# Parameters:
#     $1:             required option;
#     the remaining:  array with CLI arguments.
# Creates global array OPT_VALUES with values after option before next option.
get_range_by_option() {
    local OPT="${1}"
    local -i INDEX=2
    local -i I_START=-1
    local -i I_FINISH=-1
    for ARG in "${@:2}"; do
        ((INDEX++))
        if [[ $I_START -eq -1 ]]; then
            if [[ "$ARG" == "$OPT" ]]; then
                ((I_START=INDEX))
                continue
            fi
        elif [[ "$ARG" == -* ]]; then
            ((I_FINISH=INDEX-1))
            break
        fi
    done
    if [[ $I_START -eq -1 ]]; then
        unset OPT_VALUES
        return 0
    elif [[ $I_FINISH -eq -1 ]]; then
        ((I_FINISH=$#+1))
    fi
    local -i NUMBER_OF_ELEMENTS=$((I_FINISH-I_START))
    INDEX=0
    unset OPT_VALUES
    for ARG in "${@:I_START:NUMBER_OF_ELEMENTS}"; do
        OPT_VALUES[$INDEX]="$ARG"
        ((INDEX++))
    done
}
